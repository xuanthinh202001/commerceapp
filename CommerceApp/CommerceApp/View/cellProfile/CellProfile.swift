//
//  CellProfile.swift
//  E-CommerceApp
//
//  Created by tranxuanthinh on 5/31/22.
//

import UIKit

class CellProfile: UITableViewCell {

    @IBOutlet weak var lbData: UILabel!
    @IBOutlet weak var lbcellTitle: UILabel!
    @IBOutlet weak var imgcellProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
