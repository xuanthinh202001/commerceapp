//
//  profileVC.swift
//  E-CommerceApp
//
//  Created by tranxuanthinh on 5/30/22.
//

import UIKit
import FBSDKLoginKit
import FacebookCore
import Alamofire
import FacebookLogin
import GoogleSignIn

class profileVC: UIViewController {
    var email: String = ""
    var birthday: String = ""
    var gender: String = ""
    var id: String = ""
    var name: String = ""
    var urlgoogle: String = "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=1121934635051543&height=50&width=50&ext=1657078979&hash=AeQjkBWNbj8cZQBHL1U"
    var urlfacebook: String = ""
    @IBOutlet weak var TBVProfille: UITableView!
    @IBOutlet weak var lbText: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        fomatcode()
        
    }
    func fomatcode() {
        imgAvatar.loadFrom(URLAddress: urlfacebook)
        imgAvatar.loadImageFromUrlgoogle(urlgoogle: urlgoogle)
        imgAvatar.layer.cornerRadius = 35
        TBVProfille.register(UINib(nibName: "CellProfile", bundle: nil), forCellReuseIdentifier: "CellProfile")
        TBVProfille.dataSource = self
        TBVProfille.delegate = self
        self.lbName.text = self.name
        self.lbText.text = self.id
    }
    @IBAction func Logout(_ sender: Any) {
        let manager = LoginManager()
         manager.logOut()
        GIDSignIn.sharedInstance.signOut()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginVC") as! loginVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension profileVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TBVProfille.dequeueReusableCell(withIdentifier: "CellProfile", for: indexPath) as! CellProfile
        
        if indexPath.row == 0
        {
            cell.lbcellTitle.text = "Gender"
            cell.lbData.text = self.gender
            cell.imgcellProfile.image = UIImage(named: "gender")
        }
        if indexPath.row == 1
        {
            cell.lbcellTitle.text = "Birthday"
            cell.lbData.text = self.birthday
            cell.imgcellProfile.image = UIImage(named: "calenda")
        }
        if indexPath.row == 2
        {
            cell.lbcellTitle.text = "Email"
            cell.lbData.text = self.email
            cell.imgcellProfile.image = UIImage(named: "email")
        }
        if indexPath.row == 3
        {
            cell.lbcellTitle.text = "Phone"
            cell.lbData.text = "0969794720"
            cell.imgcellProfile.image = UIImage(named: "smartphone")
        }
        if indexPath.row == 4
        {
            cell.lbcellTitle.text = "Password"
            cell.lbData.text = "***********"
            cell.imgcellProfile.image = UIImage(named: "password")
        }
        return cell
    }
}


