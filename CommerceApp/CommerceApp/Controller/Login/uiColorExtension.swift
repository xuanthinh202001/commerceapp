//
//  uiColorExtension.swift
//  CommerceApp
//
//  Created by tranxuanthinh on 6/3/22.
//

import Foundation
import UIKit
import Alamofire
extension UIColor{
    static func colorFromHex(_ hex: String) -> UIColor {
            
            var hexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
            
            if hexString.hasPrefix("#") {
                
                hexString.remove(at: hexString.startIndex)
            }
            
            if hexString.count != 6 {
                return UIColor.magenta
            }
            
            var rgb: UInt32 = 0
            Scanner.init(string: hexString).scanHexInt32(&rgb)
            
            return UIColor.init(red: CGFloat((rgb & 0xFF0000) >> 16)/255.0,
                                green: CGFloat((rgb & 0x00FF00) >> 8)/255.0,
                                blue: CGFloat(rgb & 0x0000FF)/255.0,
                                alpha: 1.0)
        }
}
extension UIImageView {
    
    func loadImageFromUrlgoogle(urlgoogle: String) {
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]
        let request = AF.request(urlgoogle, headers: headers)
        request.responseData { (data) in
            switch data.result {
            case .success(let dataResult):
                let decodedimage = UIImage(data: dataResult)
                self.image = decodedimage
            case .failure(let error):
                print("error", error)
            }
        }
    }
}
extension UIImageView {
    func loadFrom(URLAddress: String) {
        guard let url = URL(string: URLAddress) else {
            return
        }
        
        DispatchQueue.main.async { [weak self] in
            if let imageData = try? Data(contentsOf: url) {
                if let loadedImage = UIImage(data: imageData) {
                        self?.image = loadedImage
                }
            }
        }
    }
}

