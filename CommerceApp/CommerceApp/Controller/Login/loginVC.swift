//
//  loginVC.swift
//  E-CommerceApp
//
//  Created by tranxuanthinh on 5/26/22.
//


//
import UIKit
import FBSDKLoginKit
import FacebookLogin
import Alamofire
import GoogleSignIn
import GoogleSignInSwift

class loginVC: UIViewController {
    
   
    @IBOutlet weak var btnLoginApple: UIButton!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var btnLoginGoogle: UIButton!
    @IBOutlet weak var btnLoginFacebook: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        fomat()
        Check()
    }
    
    @IBAction func setColobackgroudFacebook(_ sender: Any) {
        btnLoginFacebook.backgroundColor = UIColor.colorFromHex("#00BFFF")
        btnLoginFacebook.setTitleColor(.white, for: .normal)
    }
 
    @IBAction func LoginFacebook(_ sender: Any) {
        
        btnLoginFacebook.backgroundColor = UIColor.colorFromHex("#D3D3D3")
        btnLoginFacebook.setTitleColor(.black, for: .normal)
        
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions:["email","public_profile"], from: self, handler: {(result, error) -> Void in
    
            if (error == nil)
            {
                if let fbloginresult = result
                {
                    if(fbloginresult.isCancelled)
                    {
                        let alert = UIAlertController(title: "Facebook login", message: "Đăng nhập không thành công", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {
                            (action:UIAlertAction!) in
   
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    
                    {
                        self.getFBLoggedInUserData()
                        
                    }
                }
                
            }
        })
    }
    
    func Check()  {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"birthday,email,id,name,picture{url},gender"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if (AccessToken.current) != nil {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "profileVC") as! profileVC
                guard let json = result as? NSDictionary else { return }
                vc.name = json["name"] as! String
                vc.id = json["id"] as! String
                vc.birthday = json["birthday"] as! String
                vc.email = json["email"] as! String
                vc.gender = json["gender"] as! String
                if let profilePicObj = json["picture"] as? [String:Any] {
                    if var profilePicData = profilePicObj["data"] as? [String:Any] {
                        if var profilePic = profilePicData["url"] as? String {
                            vc.urlfacebook = "\(profilePicData)"
                            
                        }
                    }
                }
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
    func getFBLoggedInUserData()
    {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"birthday,email,id,name,picture{url},gender"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if (AccessToken.current) != nil {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "profileVC") as! profileVC
                guard let json = result as? NSDictionary else { return }
                vc.name = json["name"] as! String
                vc.id = json["id"] as! String
                vc.birthday = json["birthday"] as! String
                vc.email = json["email"] as! String
                vc.gender = json["gender"] as! String
                if let profilePicObj = json["picture"] as? [String:Any] {
                    if var profilePicData = profilePicObj["data"] as? [String:Any] {
                        if var profilePic = profilePicData["url"] as? String {
                            vc.urlfacebook = "\(profilePicData)"
                            
                        }
                    }
                }
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.navigationController?.pushViewController(vc, animated: true)
                }
        })
    }
    @IBAction func setColobackgroudGoogle(_ sender: Any) {
        btnLoginGoogle.backgroundColor = UIColor.colorFromHex("#00BFFF")
        btnLoginGoogle.setTitleColor(.white, for: .normal)
    }
    
    @IBAction func LoginGoogle(_ sender: Any) {
        handleSignInButton()
    }

    let signInConfig = GIDConfiguration(clientID: "367559754193-2oa7pa1gaftf4uk9v6lhnhqbsuoj19n0.apps.googleusercontent.com")
    func handleSignInButton() {
        btnLoginGoogle.backgroundColor = UIColor.colorFromHex("#D3D3D3")
        btnLoginGoogle.setTitleColor(.black, for: .normal)
        GIDSignIn.sharedInstance.signIn(
            with: signInConfig,
            presenting: self) { user, error in
            guard let signInUser = user else {
                return
            }
            let emailAddress = signInUser.profile?.email
            let fullName = signInUser.profile?.name
            let profilePicUrl = signInUser.profile?.imageURL(withDimension: 320)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "profileVC") as! profileVC
            vc.name = fullName!
            vc.email = emailAddress!
            if let profilePicUrl = profilePicUrl {
                vc.urlgoogle = "\(profilePicUrl)"
                
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func fomat() {
        
        viewEmail.layer.borderWidth = 1
        viewEmail.layer.borderColor = UIColor.gray.cgColor
        viewEmail.layer.cornerRadius = 10
        viewEmail.layer.masksToBounds = true
        
        viewPassword.layer.borderWidth = 1
        viewPassword.layer.borderColor = UIColor.gray.cgColor
        viewPassword.layer.cornerRadius = 10
        viewPassword.layer.masksToBounds = true

        btnLoginGoogle.layer.borderWidth = 0.5
        btnLoginGoogle.layer.borderColor = UIColor.gray.cgColor
        btnLoginGoogle.layer.cornerRadius = 10
        btnLoginGoogle.layer.masksToBounds = true

        btnLoginFacebook.layer.borderWidth = 0.5
        btnLoginFacebook.layer.borderColor = UIColor.gray.cgColor
        btnLoginFacebook.layer.cornerRadius = 10
        btnLoginFacebook.layer.masksToBounds = true

        btnLoginApple.layer.borderWidth = 0.5
        btnLoginApple.layer.borderColor = UIColor.gray.cgColor
        btnLoginApple.layer.cornerRadius = 10
        btnLoginApple.layer.masksToBounds = true
       
    }
}


